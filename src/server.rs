pub mod data {
    tonic::include_proto!("data");
}
use data::say_server::{Say, SayServer};
use data::{SayResponse,SayRequest};
use tonic::{transport::Server, Request, Response, Status};

#[derive(Debug, Default)]
pub struct MySay {}
use tokio::sync::mpsc;


#[tonic::async_trait]
impl Say for MySay {
    
    // trait Say
    async fn send(&self, request:Request<SayRequest>) ->
        Result<Response<SayResponse>, Status> {
            Ok(Response::new(SayResponse {
                message:format!("Hola {}", request.get_ref().name)
            }))
        }

    type SendStreamStream =
        tokio_stream::wrappers::ReceiverStream<Result<SayResponse, Status>>;

    async fn send_stream(&self, request:Request<SayRequest>
    ) -> Result<Response<Self::SendStreamStream>, Status> {
        println!("ListRequest = {:?}", request);
        
        let (tx, rx) = mpsc::channel(4);

        tokio::spawn(async move {
            for i in 0..4 {
                tx.send(
                    Ok(SayResponse {
                    message: format!("hello {}",i)})).await.unwrap();
            }
        });

        Ok(Response::new(
            tokio_stream::wrappers::ReceiverStream::new(rx)
        ))
            
        
    }

    type BidirectionalStream =
        tokio_stream::wrappers::ReceiverStream<Result<SayResponse, Status>>;
    
    async fn bidirectional(
        &self, request:
        Request<tonic::Streaming<SayRequest>>) ->
        Result<Response<Self::BidirectionalStream>, Status> {
            let mut streamer = request.into_inner();
            let (mut tx, rx) = mpsc::channel(4);

            tokio::spawn(async move {
                while let Some(req) = streamer.message().await.unwrap() {
                    // send data as soon it is available
                    tx.send(Ok(SayResponse{
                        message:format!("hello {}", req.name)
                    })).await;                    
                }

            });
            Ok(Response::new(
                tokio_stream::wrappers::ReceiverStream::new(rx)
            ))
    }

    
    /* implementng Receive Stream */

    async fn receive_stream(
        &self,
        request:Request<tonic::Streaming<SayRequest>>
    ) -> Result<Response<SayResponse>, Status> {
        let mut stream = request.into_inner();
        let mut message = String::from("");

        while let Some(req) = stream.message().await? {
            message.push_str(&format!("Hola {}\n", req.name))
        }

        Ok(Response::new(SayResponse { message }))
    }    
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let addr = "[::1]:50051".parse().unwrap();

    let say = MySay::default();

    println!("Server listening on {}", addr);

    Server::builder()
        .add_service(SayServer::new(say))
        .serve(addr)
        .await?;

    Ok(())
}
