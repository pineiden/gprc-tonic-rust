pub mod data {
    tonic::include_proto!("data");
}


use data::say_client::SayClient;
use data::SayRequest;
use futures::stream::iter;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let channel = tonic::transport::Channel::from_static("http://[::1]:50051")
        .connect()
        .await?;

    let mut client = SayClient::new(channel);

    // let request = tonic::Request::new(
    //     SayRequest {
    //         name:String::from("Anshul")
    //     }
    // );

    // let mut response = client.send_stream(request).await?.into_inner();
// https://dev.to/anshulgoyal15/a-beginners-guide-to-grpc-with-rust-3c7o
    // while let Some(res) = response.message().await? {
    //     println!("Response={:?}", res);        
    // }

        // creating a stream
    // procesar ina lista de consultas
    let request = tonic::Request::new(iter(vec![
        SayRequest {
            name: String::from("anshul"),
        },
        SayRequest {
            name: String::from("rahul"),
        },
        SayRequest {
            name: String::from("vijay"),
        },
    ]));
// sending stream
    //let response = client.receive_stream(request).await?.into_inner();

    // BIdirectional:
    let mut response = client.bidirectional(request).await?.into_inner();
    
    while let Some(res) = response.message().await? {
        println!("RESPONSE=\n{}", res.message);        
    }



    Ok(())

}
